package com.radwan.ahmed.education.view

import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.View
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.radwan.ahmed.education.base.BaseFragment
import com.radwan.ahmed.education.di.helper.ViewModelFactory
import com.radwan.ahmed.education.interfaces.OnClick
import kotlinx.android.synthetic.main.question_fragment.*
import javax.inject.Inject
import com.radwan.ahmed.education.R
import com.radwan.ahmed.education.adapter.ChapterAdapter
import com.radwan.ahmed.education.adapter.ReferenceQuestionAdapter
import com.radwan.ahmed.education.data.model.Chapters
import com.radwan.ahmed.education.data.model.RefQuestions
import com.radwan.ahmed.education.data.model.Reference
import com.radwan.ahmed.education.utils.KeysUtil
import com.radwan.ahmed.education.view_model.ChapterViewModel
import com.radwan.ahmed.education.view_model.RefQuestionViewModel
import kotlinx.android.synthetic.main.question_fragment.loading_view
import kotlinx.android.synthetic.main.ref_ques_frgment.*


class RefQuestionDetailsScreen : BaseFragment() {

    @Inject
    lateinit var shared: SharedPreferences
    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    var viewModel: RefQuestionViewModel? = null
    override fun layoutRes(): Int {
        return R.layout.ref_ques_frgment
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val toolbarTitle = baseActivity!!.findViewById<TextView>(R.id.toolbar_title)
        toolbarTitle.text = resources.getString(R.string.reference)
        val bundle = arguments
        if (bundle != null) {
            val category = bundle.getParcelable(KeysUtil.KEY_VALUE) ?: RefQuestions(0, "","", 0)

            toolbarTitle.text = category.title
            viewModel =
                ViewModelProviders.of(this, viewModelFactory).get(RefQuestionViewModel::class.java)
            viewModel!!.getRefQuestion(category.id)

        }
        observableViewModel()
    }

    private fun observableViewModel() {
        viewModel!!.repos.observe(this, Observer { newModel ->
            if (newModel != null) {
                tvQuestionTitle.text = newModel.refQuestions[0].title
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        tvReferenceDetails.text = Html.fromHtml(newModel.refQuestions[0].content, Html.FROM_HTML_MODE_COMPACT)
                } else {
                    tvReferenceDetails.text = Html.fromHtml(newModel.refQuestions[0].content)
                }
            }
        })
        viewModel!!.error.observe(viewLifecycleOwner, Observer { error ->
            if (error != null) {
                if (error) {
                    tvError!!.visibility = View.VISIBLE
                    tvError!!.text = "error"
                }
            }
        })

        viewModel!!.getLoading().observe(viewLifecycleOwner, Observer {
            if (it != null) {
                if (it) {
                    loading_view!!.visibility = View.VISIBLE
                } else {
                    loading_view!!.visibility = View.GONE
                }
            }
        })
    }
}
