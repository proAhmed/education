package com.radwan.ahmed.education

import android.content.SharedPreferences
import android.os.Bundle
import com.radwan.ahmed.education.Login.LoginActivity
import com.radwan.ahmed.education.base.BaseActivity
import com.radwan.ahmed.education.data.model.Category
import com.radwan.ahmed.education.data.model.User
import com.radwan.ahmed.education.utils.Transactions
import com.radwan.ahmed.education.view.CategoryScreen
import javax.inject.Inject

class SplashActivity : BaseActivity<User>() {
    @Inject
    lateinit var shared: SharedPreferences
    override fun layoutRes(): Int {
         return R.layout.activity_main
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(shared.getString("userId","")?.isNotEmpty()!!){
            intentMove(MainActivity())
            finish()
        }else {
            intentMove(LoginActivity())
        }
    }
}
