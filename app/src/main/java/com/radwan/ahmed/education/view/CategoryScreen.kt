package com.radwan.ahmed.education.view

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import com.radwan.ahmed.education.adapter.CategoryAdapter
import com.radwan.ahmed.education.base.BaseFragment
import com.radwan.ahmed.education.data.model.Category
import com.radwan.ahmed.education.di.helper.ViewModelFactory
import com.radwan.ahmed.education.interfaces.OnClick
import com.radwan.ahmed.education.utils.KeysUtil
import com.radwan.ahmed.education.view_model.CategoryViewModel
import kotlinx.android.synthetic.main.category_fragment.*
import javax.inject.Inject
import com.radwan.ahmed.education.R

class CategoryScreen : BaseFragment(), OnClick<Category> {

    var onClick: OnClick<Category>? = null
    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    var viewModel: CategoryViewModel? = null
    override fun layoutRes(): Int {
        return R.layout.category_fragment
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        onClick = this
        val toolbarTitle = baseActivity!!.findViewById<TextView>(R.id.toolbar_title)
        toolbarTitle.text = resources.getString(R.string.category)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CategoryViewModel::class.java)
        viewModel!!.getData()
//        recyclerView!!.addItemDecoration(DividerItemDecoration(baseActivity!!, DividerItemDecoration.VERTICAL))
        recyclerView!!.layoutManager = GridLayoutManager(activity, 2)
        recyclerView!!.adapter = CategoryAdapter(this, viewModel!!, this)
        observableViewModel()

    }

    private fun observableViewModel() {
        viewModel!!.repos.observe(this, Observer { newModel ->
            if (newModel != null) {
                recyclerView!!.visibility = View.VISIBLE
            }
        })
        viewModel!!.error.observe(viewLifecycleOwner, Observer { error ->
            if (error != null) {
                if (error) {
                    tv_error!!.visibility = View.VISIBLE
                    recyclerView!!.visibility = View.GONE
                    tv_error!!.text = resources.getString(R.string.error)
                }
            }
        })

        viewModel!!.getLoading().observe(viewLifecycleOwner, Observer {
            if (it != null) {
                if (it) {
                    loading_view!!.visibility = View.VISIBLE
                    tv_error!!.visibility = View.GONE
                    recyclerView!!.visibility = View.GONE
                } else {
                    loading_view!!.visibility = View.GONE
                }
            }
        })
    }

    override fun onClick(objects: Category) {
        openQuestionScreen(objects)
    }

    private fun openQuestionScreen(category: Category) {
        val bundle = Bundle()
        bundle.putParcelable(KeysUtil.KEY_VALUE, category)
        fragmentMove(ActionScreen(), bundle,"action")
    }
}
