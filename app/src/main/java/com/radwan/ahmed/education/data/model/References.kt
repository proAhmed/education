package com.radwan.ahmed.education.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class References(
    @SerializedName("max")
    @Expose val max: Boolean,
    @SerializedName("questions") @Expose
    val referenceList: List<Reference>
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readByte() != 0.toByte(),
        parcel.createTypedArrayList(Reference)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeByte(if (max) 1 else 0)
        parcel.writeTypedList(referenceList)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<References> {
        override fun createFromParcel(parcel: Parcel): References {
            return References(parcel)
        }

        override fun newArray(size: Int): Array<References?> {
            return arrayOfNulls(size)
        }
    }


}