package com.radwan.ahmed.education.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class ReferenceResponse(val status:String,
                             @SerializedName("references")
                            val references: References):Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readParcelable(References::class.java.classLoader)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(status)
        parcel.writeParcelable(references, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ReferenceResponse> {
        override fun createFromParcel(parcel: Parcel): ReferenceResponse {
            return ReferenceResponse(parcel)
        }

        override fun newArray(size: Int): Array<ReferenceResponse?> {
            return arrayOfNulls(size)
        }
    }

}