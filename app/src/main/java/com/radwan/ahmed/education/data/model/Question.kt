package com.radwan.ahmed.education.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Question(
      val id: Int, val title: String, val description: String, val answer: String
    , @SerializedName("flag_answer") val flagAnswer: Int
    , @SerializedName("category_id") val categoryId: Int
    , @SerializedName("author_id") val authorId: String
    , @SerializedName("instructor_id") val instructorId: String
    , @SerializedName("answer_file") val answerFile: String
    , val file: String, @SerializedName("date_time") val dateTime: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(title)
        parcel.writeString(description)
        parcel.writeString(answer)
        parcel.writeInt(flagAnswer)
        parcel.writeInt(categoryId)
        parcel.writeString(authorId)
        parcel.writeString(instructorId)
        parcel.writeString(file)
        parcel.writeString(answerFile)
        parcel.writeString(dateTime)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Question> {
        override fun createFromParcel(parcel: Parcel): Question {
            return Question(parcel)
        }

        override fun newArray(size: Int): Array<Question?> {
            return arrayOfNulls(size)
        }
    }
}