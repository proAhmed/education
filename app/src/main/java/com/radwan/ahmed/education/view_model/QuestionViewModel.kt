package com.radwan.ahmed.education.view_model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.radwan.ahmed.education.data.model.Question
import com.radwan.ahmed.education.data.model.QuestionDetailsResponse
import com.radwan.ahmed.education.data.model.QuestionsResponse
import com.radwan.ahmed.education.data.network.DataManager
import com.radwan.ahmed.education.utils.Transactions
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import okhttp3.Request
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject


class QuestionViewModel
@Inject
constructor() : ViewModel() {

    @Inject
    lateinit var dataManager: DataManager
     var disposable: CompositeDisposable? = null
     val newsModelMutableLiveData = MutableLiveData<QuestionsResponse>()
     val repoLoadError = MutableLiveData<Boolean>()
     val loading = MutableLiveData<Boolean>()
    @Inject
    lateinit var transactions: Transactions<Question>
    @Inject
    lateinit var context: Context


    val repos: LiveData<QuestionsResponse>
        get() = newsModelMutableLiveData
    val error: LiveData<Boolean>
        get() = repoLoadError

    init {
        disposable = CompositeDisposable()
        // getData();
    }

    fun getLoading(): LiveData<Boolean> {
        return loading
    }

    fun getData(categoryId:Int,pageNum:Int) {
        loading.value = true
        //  progressBar.setVisibility(View.VISIBLE);
        disposable!!.add(dataManager.getQuestions(categoryId,pageNum).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(object : DisposableSingleObserver<QuestionsResponse>() {
                    override fun onSuccess(value: QuestionsResponse) {
                        repoLoadError.value = false
                        if(!value.status.equals("Failed")) {
                            newsModelMutableLiveData.value = value
                        }
                        loading.value = false
                    }
                    override fun onError(e: Throwable) {
                        repoLoadError.value = true
                        loading.value = false
                    }
                }))
    }

    fun getDetailsQuestion(question_id:Int){
        loading.value = true
        disposable!!.add(dataManager.getQuestionsDetails(question_id).subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread()).subscribeWith(object :DisposableSingleObserver<QuestionDetailsResponse>(){
                override fun onSuccess(t: QuestionDetailsResponse) {
                    repoLoadError.value= false
                    loading.value = false
                }

                override fun onError(e: Throwable) {
                    repoLoadError.value= true
                    loading.value = false
                }
            }))
    }

    fun getMyQuestion(authorId:String,categoryId:Int,pageNum: Int){
        loading.value = true

        disposable!!.add(dataManager.getMyQuestions(authorId,pageNum,categoryId).subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread()).subscribeWith(object :DisposableSingleObserver<QuestionsResponse>(){
                override fun onSuccess(value: QuestionsResponse) {
                    repoLoadError.value = false
                    if(!value.status.equals("Failed",true)) {
                        newsModelMutableLiveData.value = value
                    }
                    loading.value = false
                }

                override fun onError(e: Throwable) {
                    repoLoadError.value= true
                    loading.value = false
                }
            }))
    }

    fun getDataTest(categoryId:Int) {
        loading.value = true
        var pageNum =1;
        //  progressBar.setVisibility(View.VISIBLE);


        dataManager.getQuestionTest(categoryId,pageNum).onResponse(object : Call<QuestionsResponse> {
            override fun enqueue(callback: Callback<QuestionsResponse>) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun isExecuted(): Boolean {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun clone(): Call<QuestionsResponse> {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun isCanceled(): Boolean {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun cancel() {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun execute(): Response<QuestionsResponse> {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun request(): Request {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        })



    }


override fun onCleared() {
        super.onCleared()
        if (disposable != null) {
            disposable!!.clear()
            disposable = null
        }
    }
    fun openCategory(){
//        transactions.fragmentMove(Ques())
    }
}

private fun <T> Callback<T>.onResponse(call: Call<T>) {

}


