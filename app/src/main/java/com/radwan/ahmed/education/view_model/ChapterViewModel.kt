package com.radwan.ahmed.education.view_model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.radwan.ahmed.education.data.model.*
import com.radwan.ahmed.education.data.network.DataManager
import com.radwan.ahmed.education.utils.Transactions
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import okhttp3.Request
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject


class ChapterViewModel
@Inject
constructor() : ViewModel() {

    @Inject
    lateinit var dataManager: DataManager
    var disposable: CompositeDisposable? = null
    val newsModelMutableLiveData = MutableLiveData<ChapterResponse>()
    val repoLoadError = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>()
    @Inject
    lateinit var transactions: Transactions<Question>
    @Inject
    lateinit var context: Context
    val repos: LiveData<ChapterResponse>
        get() = newsModelMutableLiveData
    val error: LiveData<Boolean>
        get() = repoLoadError

    init {
        disposable = CompositeDisposable()
        // getData();
    }

    fun getLoading(): LiveData<Boolean> {
        return loading
    }


    fun getChapters(referenceId: Int) {
        loading.value = true
        disposable!!.add(dataManager.getChapters(referenceId).subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread()).subscribeWith(object :
                DisposableSingleObserver<ChapterResponse>() {
                override fun onSuccess(value: ChapterResponse) {
                    repoLoadError.value = false
                    loading.value = false
                    newsModelMutableLiveData.value = value
                }

                override fun onError(e: Throwable) {
                    repoLoadError.value = true
                    loading.value = false
                }
            })
        )
    }


    override fun onCleared() {
        super.onCleared()
        if (disposable != null) {
            disposable!!.clear()
            disposable = null
        }
    }
}


