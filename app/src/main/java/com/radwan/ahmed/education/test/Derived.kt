package com.radwan.ahmed.education.test

class Derived(b:Base): Base by b {
     override val message = "derived by message"

}
fun main() {
    val b = BaseImp(10)
    val derived = Derived(b)
    derived.print()
    println(derived.message)
}