package com.radwan.ahmed.education.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

@Entity(tableName = "question")
data class QuestionLocal(
    @Json(name = "id")
    @PrimaryKey
    @ColumnInfo(name = "id")
    val questionId: String,
    @Json(name = "question_title")
    @ColumnInfo(name = "question_title")
    val questionTitle: String,
    @Json(name = "question_body")
    @ColumnInfo(name = "question_body")
    val questionBody: String,
    @Json(name = "question_answer")
    @ColumnInfo(name = "question_answer")
    val questionAnswer: String,
    @Json(name = "question_url")
    @ColumnInfo(name = "question_url")
    val questionUrl: String,
    @Json(name = "question_url")
    @ColumnInfo(name = "question_url")
    val questionFiles: String
)