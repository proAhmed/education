package com.radwan.ahmed.education.view

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import com.radwan.ahmed.education.base.BaseFragment
import com.radwan.ahmed.education.di.helper.ViewModelFactory
import javax.inject.Inject
import com.radwan.ahmed.education.R
import com.radwan.ahmed.education.utils.Utility
import kotlinx.android.synthetic.main.add_question.*
import android.app.Activity
import android.content.Intent
import com.radwan.ahmed.education.data.model.Question
import com.radwan.ahmed.education.utils.KeysUtil
import com.radwan.ahmed.education.utils.KeysUtil.Companion.SELECT_FILE
import com.radwan.ahmed.education.utils.Validate
import com.radwan.ahmed.education.view_model.AnswerViewModel
import okhttp3.MultipartBody


class QuestionDetailsScreen : BaseFragment() {

    @Inject
    lateinit var utility: Utility

    @Inject
    lateinit var validate: Validate
    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var question: Question
    var viewModel: AnswerViewModel? = null

    var file: MultipartBody.Part? = null
    override fun layoutRes(): Int {
        return R.layout.question_details
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val toolbarTitle = baseActivity!!.findViewById<TextView>(R.id.toolbar_title)
        toolbarTitle.text = resources.getString(R.string.add_question)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(AnswerViewModel::class.java)
        val bundle = arguments
        if (bundle != null) {
            question =
                arguments!!.getParcelable(KeysUtil.QUESTION_BUNDLE) ?: Question(7, "", "", "", 0, 0, "", "", "", "", "")
            viewModel!!.getData(question.id)
        }
        btnChooseFile.setOnClickListener {
            chooseFiles()
        }

        btnUpload.setOnClickListener {


         }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SELECT_FILE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data!!.clipData != null) {
                    val count = data.clipData!!.itemCount
                    var currentItem = 0
                    while (currentItem < count) {
                        val fileUri = data.clipData!!.getItemAt(currentItem).uri
                        //do something with the image (save it to some directory or whatever you need to do with it here)
                        file = utility.createFileFromPath(fileUri.path)
                        currentItem = currentItem + 1
                    }
                } else if (data.data != null) {
                    val filePath = data.data!!.path
                    file = utility.createFileFromPath(filePath)
                    //do something with the image (save it to some directory or whatever you need to do with it here)
                }
            }
        }
    }

    private fun chooseFiles() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*" //allows any image file type. Change * to specific extension to limit it
//**These following line is the important one!
        // intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        startActivityForResult(intent, SELECT_FILE);
    }
}
