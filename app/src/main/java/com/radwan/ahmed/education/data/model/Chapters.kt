package com.radwan.ahmed.education.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Chapters(val id:Int, @SerializedName("name_ch") val chapterName:String,
                    @SerializedName("id_ref") val  referenceId:Int) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(chapterName)
        parcel.writeInt(referenceId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Chapters> {
        override fun createFromParcel(parcel: Parcel): Chapters {
            return Chapters(parcel)
        }

        override fun newArray(size: Int): Array<Chapters?> {
            return arrayOfNulls(size)
        }
    }
}