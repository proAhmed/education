package com.radwan.ahmed.education.di.modules

import androidx.lifecycle.ViewModel
import com.radwan.ahmed.education.di.helper.ViewModelKey
import com.radwan.ahmed.education.view_model.*
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module(includes = [ViewModelFactoryModule::class])
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(QuestionViewModel::class)
    internal abstract fun bindQuestionViewModel(questionViewModel: QuestionViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CategoryViewModel::class)
    internal abstract fun bindCategoryViewModel(categoryViewModel: CategoryViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UserViewModel::class)
    internal abstract fun bindUserViewModel(userViewModel: UserViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddQuestionViewModel::class)
    internal abstract fun bindAddQuestionViewModel(addQuestionViewModel: AddQuestionViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AnswerViewModel::class)
    internal abstract fun bindAnswerViewModel(answerViewModel: AnswerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ReferenceViewModel::class)
    internal abstract fun bindReferenceViewModel(referenceViewModel: ReferenceViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChapterViewModel::class)
    internal abstract fun bindChapterViewModel(chapterViewModel: ChapterViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RefQuestionsViewModel::class)
    internal abstract fun bindRefQuestionsViewModel(refQuestionViewModel: RefQuestionsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RefQuestionViewModel::class)
    internal abstract fun bindRefQuestionViewModel(refQuestionViewModel: RefQuestionViewModel): ViewModel

}
