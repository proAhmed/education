package com.radwan.ahmed.education.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import java.util.*
import androidx.lifecycle.Observer
import com.radwan.ahmed.education.data.model.Category
import com.radwan.ahmed.education.interfaces.OnClick
import com.radwan.ahmed.education.view_model.CategoryViewModel
import com.radwan.ahmed.education.R


class CategoryAdapter(lifecycleOwner: LifecycleOwner, viewModel: CategoryViewModel, val onClickPos: OnClick<Category>) : RecyclerView.Adapter<CategoryAdapter.DataObjectHolder>() {
      private val categoryList = ArrayList<Category>()

    class DataObjectHolder(convertView: View) : RecyclerView.ViewHolder(convertView) {
        val tvTitle: TextView = convertView.findViewById(R.id.tvTitle)
        val imgSubject: ImageView = convertView.findViewById(R.id.imgSubject)
    }

    init {
        viewModel.repos.observe(lifecycleOwner, Observer{ categorys ->
            categoryList.clear()
            if (categorys != null) {
                categoryList.addAll(categorys)
                notifyDataSetChanged()
            }
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): DataObjectHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.category_item, parent, false)
        return DataObjectHolder(view)
    }

    override fun onBindViewHolder(holder: DataObjectHolder, position: Int) {
        val category = categoryList[position]
        holder.tvTitle.text = category.categoryName
        Picasso.get().load(category.categoryImage).into(holder.imgSubject)
        holder.itemView.setOnClickListener { onClickPos.onClick(category) }
    }

    override fun getItemCount(): Int {
        return categoryList.size
    }
}
