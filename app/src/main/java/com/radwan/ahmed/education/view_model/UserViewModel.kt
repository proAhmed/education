package com.radwan.ahmed.education.view_model

import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.radwan.ahmed.education.data.model.User
import com.radwan.ahmed.education.data.model.UserResponse
import com.radwan.ahmed.education.data.network.DataManager
import com.radwan.ahmed.education.utils.Transactions
import com.radwan.ahmed.education.view.CategoryScreen
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers


class UserViewModel
@Inject
 constructor(val dataManager: DataManager,val user: User) : ViewModel() {

    @Inject
    lateinit var sharedPreferences: SharedPreferences
     var disposable: CompositeDisposable? = null
     val newsModelMutableLiveData = MutableLiveData<User>()
     val repoLoadError = MutableLiveData<Boolean>()
     val loading = MutableLiveData<Boolean>()
    @Inject
    lateinit var transactions: Transactions<User>
    @Inject
    lateinit var context: Context
    val repos: LiveData<User>
        get() = newsModelMutableLiveData
    val error: LiveData<Boolean>
        get() = repoLoadError

    init {
        disposable = CompositeDisposable()
        // getData();
    }

    fun getLoading(): LiveData<Boolean> {
        return loading
    }
    fun register(user:User) {
        loading.value = true
        //  progressBar.setVisibility(View.VISIBLE);
        disposable!!.add(dataManager.register(user).subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread()).subscribeWith(object : DisposableSingleObserver<UserResponse>() {
                override fun onSuccess(value: UserResponse) {
                    repoLoadError.value = false
                    if (value.status=="success") {
                        newsModelMutableLiveData.value = value.user
                        sharedPreferences.edit().putString("userId",value.user.userId).apply()
                    }
                    loading.value = false
                }

                override fun onError(e: Throwable) {
                    repoLoadError.value = true
                    loading.value = false
                }
            }))
    }
    fun login(email:String,password:String) {
        loading.value = true
        //  progressBar.setVisibility(View.VISIBLE);
        disposable!!.add(dataManager.login(email,password).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(object : DisposableSingleObserver<UserResponse>() {
                    override fun onSuccess(value: UserResponse) {
                        repoLoadError.value = false
                        if (value.status=="success") {
                            newsModelMutableLiveData.value = value.user
                            sharedPreferences.edit().putString("userId",value.user.userId).apply()

                        }
                        loading.value = false
                    }

                    override fun onError(e: Throwable) {
                        repoLoadError.value = true
                        loading.value = false
                    }
                }))
    }


    override fun onCleared() {
        super.onCleared()
        if (disposable != null) {
            disposable!!.clear()
            disposable = null
        }
    }

}
