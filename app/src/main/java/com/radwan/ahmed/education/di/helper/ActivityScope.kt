package com.radwan.ahmed.education.di.helper

import javax.inject.Scope

@Scope
@MustBeDocumented
annotation class ActivityScope