package com.radwan.ahmed.education.utils

class KeysUtil {

    companion object {
        const val KEY_OPEN_SCREEN: String ="load_data"
        const val BASE_URL: String = "http://juniorengineer.net/"
        const val KEY_CATEGORIES: String = "admin/api/get_categories.php"
        const val KEY_LOGIN: String = "/admin/api/login.php"
        const val KEY_REGISTER: String = "/admin/api/register.php"
        const val KEY_QUESTION: String = "admin/api/get_questions.php"
        const val KEY_QUESTION_DETAILS: String = "/admin/api/get_question.php"
        const val KEY_MY_QUESTIONS:String ="/admin/api/get_myquestions.php"
        const val KEY_REFERENCE: String = "/admin/api/get_reference.php"
        const val KEY_REF_CHAPTERS: String = "/admin/api/get_chapter.php"
        const val KEY_REF_QUESTIONS: String = "/admin/api/get_ref_questions.php"
        const val KEY_REF_QUESTION: String = "/admin/api/get_ref_question.php"
        const val KEY_ADD_ANSWER: String = "/admin/api/add_answer.php"
        const val KEY_ADD_QUESTION: String = "admin/api/add_question.php"
        const val KEY_ID: String = "id"
        const val QUESTION_BUNDLE: String = "questions"
        const val KEY_VALUE = "delivery"
        const val KEY_FILE = "file"
        const val KEY_AUTHOR_ID = "author_id"
        const val KEY_CATEGORY_ID = "category_id"
        const val KEY_QUESTION_ID = "question_id"
        const val KEY_REF_QUESTION_ID = "qu_id"
        const val KEY_QUESTION_DETAILS_ID = "qu_id"
        const val KEY_REFERENCE_ID = "ref_id"
        const val KEY_CHAPTER_ID = "ch_id"
        const val KEY_ANSWER = "answer"
        const val KEY_INSTRUCTOR_ID = "instructor_id"
        const val KEY_TITLE = "title"
        const val KEY_DESCRIPTION = "description"
        const val KEY_PAGE_NUM: String = "page_number"
        const val KEY_USER_TYPE = "userType"
        const val KEY_USER_ID = "userId"

        const val SELECT_FILE = 300
    }
}