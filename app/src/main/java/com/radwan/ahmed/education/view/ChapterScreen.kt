package com.radwan.ahmed.education.view

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.radwan.ahmed.education.base.BaseFragment
import com.radwan.ahmed.education.di.helper.ViewModelFactory
import com.radwan.ahmed.education.interfaces.OnClick
import kotlinx.android.synthetic.main.question_fragment.*
import javax.inject.Inject
import com.radwan.ahmed.education.R
import com.radwan.ahmed.education.adapter.ChapterAdapter
import com.radwan.ahmed.education.data.model.Chapters
import com.radwan.ahmed.education.data.model.Reference
import com.radwan.ahmed.education.utils.KeysUtil
import com.radwan.ahmed.education.view_model.ChapterViewModel


class ChapterScreen : BaseFragment(), OnClick<Chapters> {

    @Inject
    lateinit var shared: SharedPreferences
    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    var viewModel: ChapterViewModel? = null
    override fun layoutRes(): Int {
        return R.layout.question_fragment
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val toolbarTitle = baseActivity!!.findViewById<TextView>(R.id.toolbar_title)
       toolbarTitle.text = resources.getString(R.string.reference)
        val bundle = arguments
        if (bundle != null) {
            val reference = bundle.getParcelable(KeysUtil.KEY_VALUE) ?: Reference(0, "", "","","")

            toolbarTitle.text = reference.refName
            viewModel =
                ViewModelProviders.of(this, viewModelFactory).get(ChapterViewModel::class.java)
                viewModel!!.getChapters(reference.refId)


        recyclerView!!.layoutManager = LinearLayoutManager(activity)
        observableViewModel()
        recyclerView!!.adapter = ChapterAdapter(this,reference.refName, viewModel!!, this)
    }
}
      fun observableViewModel() {
        viewModel!!.repos.observe(this, Observer{ newModel ->
            if (newModel != null) {
                recyclerView!!.visibility = View.VISIBLE
            }
        })
        viewModel!!.error.observe(viewLifecycleOwner, Observer{ error ->
            if (error != null) {
                if (error) {
                    tv_error!!.visibility = View.VISIBLE
                    recyclerView!!.visibility = View.GONE
                    tv_error!!.text = "error"
                }
            }
        })

        viewModel!!.getLoading().observe(viewLifecycleOwner, Observer {
            if (it != null) {
                if (it) {
                    loading_view!!.visibility = View.VISIBLE
                    tv_error!!.visibility = View.GONE
                    recyclerView!!.visibility = View.GONE
                } else {
                    loading_view!!.visibility = View.GONE
                }
            }
        })
    }

    override fun onClick(objects: Chapters) {
        val bundle = Bundle()
        bundle.putParcelable(KeysUtil.KEY_VALUE, objects)
        val fragment = RefQuestionsScreen()
        fragmentMove(fragment, bundle, "Chapter")
    }

}
