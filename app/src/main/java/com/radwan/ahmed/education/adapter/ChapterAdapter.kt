package com.radwan.ahmed.education.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import java.util.*
import androidx.lifecycle.Observer
import com.radwan.ahmed.education.data.model.Question
import com.radwan.ahmed.education.interfaces.OnClick
import com.radwan.ahmed.education.view_model.QuestionViewModel
import com.radwan.ahmed.education.R
import com.radwan.ahmed.education.data.model.Chapters
import com.radwan.ahmed.education.data.model.Reference
import com.radwan.ahmed.education.view_model.ChapterViewModel
import com.radwan.ahmed.education.view_model.ReferenceViewModel

/**
 * Created by ahmed on 1/06/2018.
 */
class ChapterAdapter(lifecycleOwner: LifecycleOwner,val referenceName:String, viewModel: ChapterViewModel, val onClickPos: OnClick<Chapters>)
    : RecyclerView.Adapter<ChapterAdapter.DataObjectHolder>() {
      private val referenceList = ArrayList<Chapters>()

    class DataObjectHolder(convertView: View) : RecyclerView.ViewHolder(convertView) {
        val tvTitle: TextView = convertView.findViewById(R.id.tvTitle)
        val tvDetails: TextView = convertView.findViewById(R.id.tvDetails)
    }

    init {
        viewModel.repos.observe(lifecycleOwner, Observer{ references ->
            referenceList.clear()
            if (references?.chapters != null) {
                referenceList.addAll(references.chapters)
                notifyDataSetChanged()
            }
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): DataObjectHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.ref_ch_item, parent, false)
        return DataObjectHolder(view)
    }

    override fun onBindViewHolder(holder: DataObjectHolder, position: Int) {
        val question = referenceList[position]
        holder.tvTitle.text = question.chapterName
        holder.tvDetails.text = referenceName
        holder.itemView.setOnClickListener { onClickPos.onClick(question) }
    }

    override fun getItemCount(): Int {
        return referenceList.size
    }
}
