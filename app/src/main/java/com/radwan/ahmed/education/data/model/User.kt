package com.radwan.ahmed.education.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import javax.inject.Inject

data class User  constructor(@SerializedName("u_id")var userId:String,
                             @SerializedName("fname")var userFName:String,
                             @SerializedName("lname")var userLName:String,
                             @SerializedName("phone")var userPhone:String,
                             @SerializedName("email")var userEmail:String,
                             @SerializedName("password")var userPassword:String,
                             @SerializedName("type")var userType:Int
):Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readInt()
    )
    @Inject constructor() : this("", "",
        "", "",
        "", "",0
    )
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(userId)
        parcel.writeString(userFName)
        parcel.writeString(userLName)
        parcel.writeString(userPhone)
        parcel.writeString(userEmail)
        parcel.writeString(userPassword)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }
}