package com.radwan.ahmed.education.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class QuestionDetailsResponse(val status:String,
                                   @SerializedName("question")
                            val question: Question):Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readParcelable(Question::class.java.classLoader)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(status)
        parcel.writeParcelable(question, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<QuestionDetailsResponse> {
        override fun createFromParcel(parcel: Parcel): QuestionDetailsResponse {
            return QuestionDetailsResponse(parcel)
        }

        override fun newArray(size: Int): Array<QuestionDetailsResponse?> {
            return arrayOfNulls(size)
        }
    }

}