package com.radwan.ahmed.education

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import com.radwan.ahmed.education.base.BaseActivity
import com.radwan.ahmed.education.data.model.Category
import com.radwan.ahmed.education.data.model.User
import com.radwan.ahmed.education.utils.KeysUtil
import com.radwan.ahmed.education.utils.Transactions
import com.radwan.ahmed.education.view.CategoryScreen
import javax.inject.Inject

class MainActivity : BaseActivity<User>() {
    @Inject
    lateinit var shared: SharedPreferences
    override fun layoutRes(): Int {
         return R.layout.activity_main
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent= intent
        val name =intent.getStringExtra("name")
        val path =intent.getStringExtra("path")
        shared.edit().putString("name",name).apply()
        shared.edit().putString("path",path).apply()
        fragmentMove(CategoryScreen(),"category")
    }
}
