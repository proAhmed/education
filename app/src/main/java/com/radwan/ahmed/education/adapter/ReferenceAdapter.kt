package com.radwan.ahmed.education.adapter

import android.annotation.SuppressLint
import android.graphics.ColorFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import java.util.*
import androidx.lifecycle.Observer
import com.radwan.ahmed.education.data.model.Question
import com.radwan.ahmed.education.interfaces.OnClick
import com.radwan.ahmed.education.view_model.QuestionViewModel
import com.radwan.ahmed.education.R
import com.radwan.ahmed.education.data.model.Reference
import com.radwan.ahmed.education.view_model.ReferenceViewModel
import setTint

/**
 * Created by ahmed on 1/06/2018.
 */
class ReferenceAdapter(lifecycleOwner: LifecycleOwner, viewModel: ReferenceViewModel, private val onClickPos: OnClick<Reference>)
    : RecyclerView.Adapter<ReferenceAdapter.DataObjectHolder>() {
      private val referenceList = ArrayList<Reference>()

    class DataObjectHolder(convertView: View) : RecyclerView.ViewHolder(convertView) {
        val tvTitle: TextView = convertView.findViewById(R.id.tvTitle)
        val tvDetails: TextView = convertView.findViewById(R.id.tvDetails)
        val tvFCharacter:TextView = convertView.findViewById(R.id.tvFCharacter)
        val imgFCh:ImageView = convertView.findViewById(R.id.imgFCh)
    }

    init {
        viewModel.repos.observe(lifecycleOwner, Observer{ references ->
            referenceList.clear()
            if (references?.references != null) {
                referenceList.addAll(references.references.referenceList)
                notifyDataSetChanged()
            }
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): DataObjectHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.reference_item, parent, false)
        return DataObjectHolder(view)
    }

    override fun onBindViewHolder(holder: DataObjectHolder, position: Int) {
        val question = referenceList[position]
        holder.tvTitle.text = question.refName
        holder.tvDetails.text = question.contentEn
        holder.tvFCharacter.text = question.refName.substring(0,1)
        holder.itemView.setOnClickListener {
            onClickPos.onClick(question)
        }

       // holder.imgFCh.setTint(holder.itemView.context.resources.getColor(R.color.blue));
       
    }

    override fun getItemCount(): Int {
        return referenceList.size
    }
}


