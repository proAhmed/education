package com.radwan.ahmed.education.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import com.radwan.ahmed.education.data.model.Question
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import android.content.ClipData.Item
import androidx.recyclerview.widget.DiffUtil
import android.widget.Toast
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import androidx.annotation.NonNull
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.radwan.ahmed.education.R
import com.radwan.ahmed.education.interfaces.OnClick
import com.radwan.ahmed.education.view_model.QuestionViewModel
import java.util.ArrayList


class ItemAdapter(val context: Context, lifecycleOwner: LifecycleOwner, viewModel: QuestionViewModel,
                   private val onClickPos: OnClick<Question>):
    PagedListAdapter<Question, ItemAdapter.ItemViewHolder>(DIFF_CALLBACK) {

    private val questionList = ArrayList<Question>()

    override fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.question_item, parent, false)
        return ItemViewHolder(view)
    }
    init {
        viewModel.repos.observe(lifecycleOwner, Observer{ questionLists ->
            questionList.clear()
            if (questionLists != null) {
                questionList.addAll(questionLists.questions.questions)
                notifyDataSetChanged()
            }
        })
    }
    companion object {
        private val DIFF_CALLBACK = object :
            DiffUtil.ItemCallback<Question>() {
            // Concert details may have changed if reloaded from the database,
            // but ID is fixed.
            override fun areItemsTheSame(oldConcert: Question,
                                         newConcert: Question) = oldConcert.id == newConcert.id

            override fun areContentsTheSame(oldConcert: Question,
                                            newConcert: Question) = oldConcert == newConcert
        }
    }
    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val question = questionList[position]

        if (question != null) {
            holder.tvTitle.text = question.title
            holder.tvDetails.text = question.description
            holder.itemView.setOnClickListener {
                onClickPos.onClick(question)
            }
            if(question.flagAnswer==0){
                holder.btnAnsweredState.setCompoundDrawablesWithIntrinsicBounds( R.drawable.close, 0, 0, 0);
                holder.btnAnsweredState.setBackgroundResource(R.drawable.radius_question)

            }else{
                holder.btnAnsweredState.setCompoundDrawablesWithIntrinsicBounds( R.drawable.checked, 0, 0, 0);
            }
        } else {
            Toast.makeText(context, "Item is null", Toast.LENGTH_LONG).show()
        }
    }

    private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Item>() {
        override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
            return oldItem.text === newItem.text
        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
            return oldItem == newItem
        }
    }

    inner class ItemViewHolder(convertView: View) : RecyclerView.ViewHolder(convertView) {


             val tvTitle: TextView = convertView.findViewById(R.id.tvTitle)
            val tvDetails: TextView = convertView.findViewById(R.id.tvDetails)
            val btnAnsweredState: Button = convertView.findViewById(R.id.btnAnsweredState)
     }

}