package com.radwan.ahmed.education.view_model

import android.content.Context
import android.os.Bundle
import javax.inject.Inject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.radwan.ahmed.education.data.model.Category
import com.radwan.ahmed.education.data.model.CategoryResponse
import com.radwan.ahmed.education.data.network.DataManager
import com.radwan.ahmed.education.utils.KeysUtil
import com.radwan.ahmed.education.utils.Transactions
import com.radwan.ahmed.education.view.QuestionScreen
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers


class CategoryViewModel
@Inject
constructor() : ViewModel() {

    @Inject
    lateinit var dataManager: DataManager;
     var disposable: CompositeDisposable? = null
     val newsModelMutableLiveData = MutableLiveData<List<Category>>()
     val repoLoadError = MutableLiveData<Boolean>()
     val loading = MutableLiveData<Boolean>()
    @Inject
    lateinit var transactions: Transactions<Category>
    @Inject
    lateinit var context: Context
    val repos: LiveData<List<Category>>
        get() = newsModelMutableLiveData
    val error: LiveData<Boolean>
        get() = repoLoadError

    init {
        disposable = CompositeDisposable()
        // getData();
    }

    fun getLoading(): LiveData<Boolean> {
        return loading
    }

    fun getData() {
        loading.value = true
        //  progressBar.setVisibility(View.VISIBLE);
        disposable!!.add(dataManager.getCategories().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(object : DisposableSingleObserver<CategoryResponse>() {
                    override fun onSuccess(value: CategoryResponse) {
                        repoLoadError.value = false
                        newsModelMutableLiveData.value = value.list
                        loading.value = false
                    }

                    override fun onError(e: Throwable) {
                        repoLoadError.value = true
                        loading.value = false
                    }
                }))
    }


    override fun onCleared() {
        super.onCleared()
        if (disposable != null) {
            disposable!!.clear()
            disposable = null
        }
    }

}
