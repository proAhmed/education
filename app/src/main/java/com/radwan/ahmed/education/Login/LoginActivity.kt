package com.radwan.ahmed.education.Login

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.radwan.ahmed.education.MainActivity
import com.radwan.ahmed.education.base.BaseActivity
import com.radwan.ahmed.education.di.helper.ViewModelFactory
import com.radwan.ahmed.education.view_model.UserViewModel
import kotlinx.android.synthetic.main.activity_login.*
import com.radwan.ahmed.education.R
import com.radwan.ahmed.education.data.model.User
import com.radwan.ahmed.education.utils.KeysUtil
import javax.inject.Inject

class LoginActivity : BaseActivity<User>() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    @Inject
    lateinit var shared:SharedPreferences
    var viewModel: UserViewModel? = null
    override fun layoutRes(): Int {
       return R.layout.activity_login
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(shared.getString("userId","")?.isNotEmpty()!!){
            intentMove(MainActivity())
            finish()
        }
        edEmail.text.toString()
//        btnRegister
        btnLogin.setOnClickListener {
            viewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel::class.java)
            viewModel!!.login(edEmail.text.toString(),edPass.text.toString())
            observableViewModel()
        }

        tvRegister.setOnClickListener{
            val user = User()
            intentMove(RegisterActivity(),user)
        }

    }

    private fun observableViewModel() {
        viewModel!!.repos.observe(this, Observer{ user ->
            if (!user.userId.isBlank()) {
                shared.edit().putString(KeysUtil.KEY_USER_ID,user.userId).apply()
                shared.edit().putInt(KeysUtil.KEY_USER_TYPE,user.userType).apply()
                intentMove(MainActivity(),user)
                finish()
            }

        })
        viewModel!!.error.observe(this, Observer{ error ->
            if (error != null) {
                if (error) {

                }
            }
        })

        viewModel!!.getLoading().observe(this, Observer {
            if (it != null) {
                if (it) {
                    loading_view!!.visibility = View.VISIBLE
                } else {
                    loading_view!!.visibility = View.GONE

                }
            }
        })
    }

}
