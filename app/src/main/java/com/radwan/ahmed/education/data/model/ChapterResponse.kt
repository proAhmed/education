package com.radwan.ahmed.education.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class ChapterResponse(val status:String,
                           @SerializedName("chapters")
                            val chapters: List<Chapters>):Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.createTypedArrayList(Chapters)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(status)
        parcel.writeTypedList(chapters)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ChapterResponse> {
        override fun createFromParcel(parcel: Parcel): ChapterResponse {
            return ChapterResponse(parcel)
        }

        override fun newArray(size: Int): Array<ChapterResponse?> {
            return arrayOfNulls(size)
        }
    }
}

