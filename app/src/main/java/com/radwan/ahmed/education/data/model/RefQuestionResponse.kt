package com.radwan.ahmed.education.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class RefQuestionResponse(val status:String,
                               @SerializedName("question")
                            val refQuestions: List<RefQuestions>):Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.createTypedArrayList(RefQuestions)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(status)
        parcel.writeTypedList(refQuestions)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RefQuestionResponse> {
        override fun createFromParcel(parcel: Parcel): RefQuestionResponse {
            return RefQuestionResponse(parcel)
        }

        override fun newArray(size: Int): Array<RefQuestionResponse?> {
            return arrayOfNulls(size)
        }
    }
}

