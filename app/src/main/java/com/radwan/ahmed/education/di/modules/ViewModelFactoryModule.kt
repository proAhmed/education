package com.radwan.ahmed.education.di.modules

import androidx.lifecycle.ViewModelProvider
import com.radwan.ahmed.education.di.helper.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelFactoryModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}