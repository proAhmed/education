package com.radwan.ahmed.education.di.modules


import com.radwan.ahmed.education.Login.LoginActivity
import com.radwan.ahmed.education.Login.RegisterActivity
import com.radwan.ahmed.education.MainActivity
import com.radwan.ahmed.education.SplashActivity
import com.radwan.ahmed.education.base.BaseActivity
import dagger.Binds
import dagger.BindsInstance
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap


@Module(includes = arrayOf(ViewModelModule::class))
abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = [MainFragmentBindingModule::class])
    internal abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [MainFragmentBindingModule::class])
    internal abstract fun bindLoginActivity(): LoginActivity

//    @Binds
//    internal abstract fun bindBaseActivity(): BaseActivity

//    @ContributesAndroidInjector(modules = [MainFragmentBindingModule::class])
//    internal abstract fun bindResetPasswordActivity(): ResetPasswordActivity

    @ContributesAndroidInjector(modules = [MainFragmentBindingModule::class])
    internal abstract fun bindRegisterActivity(): RegisterActivity

    @ContributesAndroidInjector(modules = [MainFragmentBindingModule::class])
    internal abstract fun binSplashActivity(): SplashActivity

//    @ContributesAndroidInjector(modules = [MainFragmentBindingModule::class])
//    internal abstract fun bindNewsDetails(): NewsDetails
}
