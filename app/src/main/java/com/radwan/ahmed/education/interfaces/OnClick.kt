package com.radwan.ahmed.education.interfaces

interface OnClick<T> {
    fun onClick(objects: T)
}