package com.radwan.ahmed.education.view

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentResolver
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.hbisoft.pickit.PickiTCallbacks
import com.radwan.ahmed.education.base.BaseFragment
import com.radwan.ahmed.education.data.model.Question
import com.radwan.ahmed.education.di.helper.ViewModelFactory
import com.radwan.ahmed.education.utils.*
import com.radwan.ahmed.education.view_model.AnswerViewModel
import kotlinx.android.synthetic.main.answer_frgment.*
import kotlinx.android.synthetic.main.fra_answered.*
import kotlinx.android.synthetic.main.fra_not_answered_ins.*
import kotlinx.android.synthetic.main.question_fragment.loading_view
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import javax.inject.Inject
import com.hbisoft.pickit.PickiT
import android.os.Build
import android.Manifest.permission
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.pm.PackageManager
import com.radwan.ahmed.education.MainActivity
import android.Manifest.permission.READ_EXTERNAL_STORAGE
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import android.widget.Toast
import androidx.annotation.NonNull
import com.radwan.ahmed.education.R
import com.radwan.ahmed.education.utils.KeysUtil.Companion.SELECT_FILE


class AnswerScreen : BaseFragment(), PickiTCallbacks {
    override fun PickiTonProgressUpdate(progress: Int) {
    }

    override fun PickiTonStartListener() {
    }

    override fun PickiTonCompleteListener(
        path: String?,
        wasDriveFile: Boolean,
        wasUnknownProvider: Boolean,
        wasSuccessful: Boolean,
        Reason: String?
    ) {
              file = utility.createFileFromPath(path,type)
    }
fun getName(){
    //https://stackoverflow.com/questions/3401579/get-filename-and-path-from-uri-from-mediastore
}

    @Inject
    lateinit var validate: Validate
    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    var viewModel: AnswerViewModel? = null
    @Inject
    lateinit var shared:SharedPreferences
    var file: MultipartBody.Part? = null
    @Inject
    lateinit var utility: Utility
    lateinit var pickiT:PickiT
    lateinit var type:String
    override fun layoutRes(): Int {
        return R.layout.answer_frgment
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val toolbarTitle = baseActivity!!.findViewById<TextView>(R.id.toolbar_title)
        val bundle = arguments
        if (bundle != null) {
            val question = arguments!!.getParcelable(KeysUtil.KEY_VALUE) ?: Question(
                7, "", "", ""
                , 0, 0, "", "", "", "", ""
            )
            pickiT = PickiT(activity, this)

            toolbarTitle.text = question.title
            viewModel =
                ViewModelProviders.of(this, viewModelFactory).get(AnswerViewModel::class.java)
            viewModel!!.getData(question.id)
            observableViewModel()
            btnOpenQues.setOnClickListener {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(question.file))
                if(browserIntent.resolveActivity(activity?.packageManager) != null) {
                    activity?.startActivity(browserIntent)
                }
            }
            btnChooseFile.setOnClickListener {
                if(question.flagAnswer==0){
                    chooseFiles();
                }
            }
            btnOpenAns.setOnClickListener {

                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(question.answerFile))
                    if(browserIntent.resolveActivity(activity?.packageManager) != null) {
                        activity?.startActivity(browserIntent)
                }
            }
            btnAddAnswer.setOnClickListener {
                if(validate.validateText(edAnswer.text.toString()))

                    viewModel!!.addAnswer(shared.getString("userId",""), question.id,edAnswer.text.toString(),file)
            }

        }
    }

    @SuppressLint("SetTextI18n")
    fun observableViewModel() {
        viewModel!!.repos.observe(this, Observer { newModel ->
            if (newModel != null) {

                tvQuestionTitle.text = newModel.question.title
                tvQuestionDetails.text = newModel.question.description
                if(newModel.question.flagAnswer==1){
                    fraAnswered.visibility = View.VISIBLE
                    tvAnswerDetails.text = newModel.question.answer
                    btnOpenAns.visibility = View.VISIBLE
                }else {
                    btnOpenQues.setBackgroundResource(R.drawable.radius_question)
                    if(shared.getInt(KeysUtil.KEY_USER_TYPE,0)==2){
                        fraNotAnsStu.visibility = View.VISIBLE
                    }else{
                        fraNotAnsIns.visibility = View.VISIBLE

                    }
                }
            }
        })
        viewModel!!.error.observe(viewLifecycleOwner, Observer { error ->
            if (error != null) {
                if (error) {
                    tvError!!.visibility = View.VISIBLE
                    tvError!!.text = "error"
                }
            }
        })

        viewModel!!.getLoading().observe(viewLifecycleOwner, Observer {
            if (it != null) {
                if (it) {
                    loading_view!!.visibility = View.VISIBLE
                    tvError!!.visibility = View.GONE
                } else {
                    loading_view!!.visibility = View.GONE
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == KeysUtil.SELECT_FILE) {
            if (resultCode == Activity.RESULT_OK) {
                type = activity?.contentResolver?.getType(data?.data!!)!!
                pickiT.getPath(data!!.data, Build.VERSION.SDK_INT);
            }
        }
    }
    private fun chooseFiles(){
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*"
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra("return-data", true);
        startActivityForResult(intent, KeysUtil.SELECT_FILE);
    }
    override fun onDestroy() {
        super.onDestroy()
            pickiT.deleteTemporaryFile()
    }
    private fun checkSelfPermission(): Boolean {
        if (ContextCompat.checkSelfPermission(
                activity!!,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                activity!!,
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ),
                SELECT_FILE
            )
            return false
        }
        return true
    }

    //  Handle permissions
    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        if (requestCode == SELECT_FILE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //  Permissions was granted, open the gallery
                chooseFiles()
            } else {
                Toast.makeText(activity,"No permission for " + Manifest.permission.READ_EXTERNAL_STORAGE,Toast.LENGTH_LONG).show()
            }//  Permissions was not granted
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        pickiT.deleteTemporaryFile();

    }
}
