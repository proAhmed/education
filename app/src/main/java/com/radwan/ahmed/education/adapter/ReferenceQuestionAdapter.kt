package com.radwan.ahmed.education.adapter

import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import java.util.*
import androidx.lifecycle.Observer
import com.radwan.ahmed.education.interfaces.OnClick
import com.radwan.ahmed.education.R
import com.radwan.ahmed.education.data.model.RefQuestions
import com.radwan.ahmed.education.view_model.RefQuestionViewModel
import com.radwan.ahmed.education.view_model.RefQuestionsViewModel
import kotlinx.android.synthetic.main.ref_ques_frgment.*

/**
 * Created by ahmed on 1/06/2018.
 */
class ReferenceQuestionAdapter(lifecycleOwner: LifecycleOwner, viewModel: RefQuestionsViewModel, val onClickPos: OnClick<RefQuestions>) : RecyclerView.Adapter<ReferenceQuestionAdapter.DataObjectHolder>() {
      private val questionList = ArrayList<RefQuestions>()

    class DataObjectHolder(convertView: View) : RecyclerView.ViewHolder(convertView) {
        val tvTitle: TextView = convertView.findViewById(R.id.tvTitle)
        val tvDetails: TextView = convertView.findViewById(R.id.tvDetails)
    }

    init {
        viewModel.repos.observe(lifecycleOwner, Observer{ questionLists ->
            questionList.clear()
            if (questionLists != null) {
                questionList.addAll(questionLists.refQuestions)
                notifyDataSetChanged()
            }
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): DataObjectHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.ref_ch_ques_item, parent, false)
        return DataObjectHolder(view)
    }

    override fun onBindViewHolder(holder: DataObjectHolder, position: Int) {
        val question = questionList[position]
        holder.tvTitle.text = question.title
         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.tvDetails.text = Html.fromHtml(question.content, Html.FROM_HTML_MODE_COMPACT)
        } else {
            holder.tvDetails.text = Html.fromHtml(question.content)
        }
        holder.itemView.setOnClickListener { onClickPos.onClick(question) }
    }

    override fun getItemCount(): Int {
        return questionList.size
    }
}
