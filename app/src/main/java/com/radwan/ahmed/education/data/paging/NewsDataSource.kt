package com.radwan.ahmed.education.data.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.radwan.ahmed.education.data.model.Question
import com.radwan.ahmed.education.data.model.State
import com.radwan.ahmed.education.data.network.ApiInterface
import com.radwan.ahmed.education.data.network.DataManager
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class NewsDataSource(
    private val networkService: DataManager,
    private val compositeDisposable: CompositeDisposable)
    : PageKeyedDataSource<Int, Question>() {

        var state: MutableLiveData<State> = MutableLiveData()
        private var retryCompletable: Completable? = null


        override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Question>) {
            updateState(State.LOADING)
            compositeDisposable.add(
                networkService.getQuestions(7, params.requestedLoadSize)
                    .subscribe(
                        { response ->
                            updateState(State.DONE)
                            callback.onResult(response.questions,
                                null,
                                2
                            )
                        },
                        {
                            updateState(State.ERROR)
                            setRetry(Action { loadInitial(params, callback) })
                        }
                    )
            )
        }

        override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Question>) {
            updateState(State.LOADING)
            compositeDisposable.add(
                networkService.getNews(params.key, params.requestedLoadSize)
                    .subscribe(
                        { response ->
                            updateState(State.DONE)
                            callback.onResult(response.news,
                                params.key + 1
                            )
                        },
                        {
                            updateState(State.ERROR)
                            setRetry(Action { loadAfter(params, callback) })
                        }
                    )
            )
        }

        override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Question>) {
        }

        private fun updateState(state: State) {
            this.state.postValue(state)
        }

        fun retry() {
            if (retryCompletable != null) {
                compositeDisposable.add(retryCompletable!!
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe())
            }
        }

        private fun setRetry(action: Action?) {
            retryCompletable = if (action == null) null else Completable.fromAction(action)
        }

    }