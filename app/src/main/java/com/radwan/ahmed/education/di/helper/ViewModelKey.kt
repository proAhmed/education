package com.radwan.ahmed.education.di.helper


import kotlin.annotation.Retention

import androidx.lifecycle.ViewModel
import dagger.MapKey
import kotlin.reflect.KClass

//@MustBeDocumented
//@MapKey
//@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
//@Retention(AnnotationRetention.RUNTIME)
//annotation class ViewModelKey(val value: KClass<out ViewModel>)


@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@MapKey
annotation class ViewModelKey(val value: KClass<out ViewModel>)