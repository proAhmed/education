package com.radwan.ahmed.education.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.core.app.ActivityCompat
import com.radwan.ahmed.education.utils.KeysUtil.Companion.KEY_FILE
import javax.inject.Inject
import java.io.File
import okhttp3.MultipartBody
import okhttp3.MediaType
import okhttp3.RequestBody
import android.provider.MediaStore






class  Utility @Inject constructor() {

@Inject
lateinit var context:Context
    fun createFileFromPath(urlPath:String?): MultipartBody.Part? {
        if(urlPath!=null) {
            val file = File(urlPath)
            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), urlPath)
            val filePart = MultipartBody.Part.createFormData(
                "file",
                file.name,
                RequestBody.create(MediaType.parse("image/*"), file)
            )

        val requestBody = RequestBody.create(MediaType.parse("application/pdf"), file)
        val filename = RequestBody.create(MediaType.parse("image/*"), file.name)
      return MultipartBody.Part.createFormData(KEY_FILE, file.name, requestBody) }
        return null;
    }
    fun createFileFromPath(urlPath:String?,uri: String?): MultipartBody.Part? {
        if(urlPath!=null) {
            val file = File(urlPath!!)
            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), urlPath)
            val filePart = MultipartBody.Part.createFormData(
                "file",
                file.name,
                RequestBody.create(MediaType.parse("image/*"), file)
            )

            val requestBody = RequestBody.create(MediaType.parse(uri), file)
            val filename = RequestBody.create(MediaType.parse("image/*"), file.name)
            return MultipartBody.Part.createFormData(KEY_FILE, file.name, requestBody) }
        return null;
    }
    fun convertToRequestBody(input:String):RequestBody{
       return RequestBody.create(MediaType.parse("text/plain"), input)

    }
    fun getPath(uri: Uri): String {
        val projection = arrayOf(MediaStore.MediaColumns.DATA)
        val cursor = context.contentResolver?.query(uri, projection, null, null, null)
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            val column_index = cursor
                .getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)
            cursor?.moveToFirst()
            return cursor?.getString(column_index)
        } else
            return ""
    }
}