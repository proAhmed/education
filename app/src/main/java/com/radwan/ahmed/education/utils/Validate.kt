package com.radwan.ahmed.education.utils

import javax.inject.Inject

class Validate @Inject constructor() {
    fun validateText(text:String):Boolean{
        return text.isNotEmpty()
    }
}