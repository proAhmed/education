package com.radwan.ahmed.education.view_model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.radwan.ahmed.education.data.model.AddQuestionResponse
import com.radwan.ahmed.education.data.network.DataManager
import com.radwan.ahmed.education.utils.Utility
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody
import javax.inject.Inject


class AddQuestionViewModel
@Inject
constructor() : ViewModel() {

    @Inject
    lateinit var dataManager: DataManager
     var disposable: CompositeDisposable? = null
     val addQuestionMutableLiveData = MutableLiveData<AddQuestionResponse>()
     val repoLoadError = MutableLiveData<Boolean>()
     val loading = MutableLiveData<Boolean>()
    val response = MutableLiveData<String>()
    @Inject
    lateinit var utility: Utility

    @Inject
    lateinit var context: Context
    val repos: LiveData<AddQuestionResponse>
        get() = addQuestionMutableLiveData
    val resp: LiveData<String>
        get() = response
    val error: LiveData<Boolean>
        get() = repoLoadError

    init {
        disposable = CompositeDisposable()
        // getData();
    }

    fun getLoading(): LiveData<Boolean> {
        return loading
    }
   lateinit var datass:Single<AddQuestionResponse>
    fun addQuestion(authorId:String, categoryId:Int, title: String, body:String, file: MultipartBody.Part?) {
        loading.value = true
        //  progressBar.setVisibility(View.VISIBLE);

        if(file!=null){
           datass =  dataManager.addQuestions(utility.convertToRequestBody(authorId),
               utility.convertToRequestBody(categoryId.toString())
               ,utility.convertToRequestBody(title)
               ,utility.convertToRequestBody(body),file)
        }else{
            datass =  dataManager.addQuestions(authorId
                ,categoryId
                ,title
                ,body)

        }
        disposable!!.add(datass.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(object : DisposableSingleObserver<AddQuestionResponse>() {
                    override fun onSuccess(value: AddQuestionResponse) {
                        repoLoadError.value = false
                        addQuestionMutableLiveData.value = value
                        loading.value = false
                        response.postValue(value.status);
                    }
                    override fun onError(e: Throwable) {
                        repoLoadError.value = true
                        loading.value = false
                    }
                }))
    }

    override fun onCleared() {
        super.onCleared()
        if (disposable != null) {
            disposable!!.clear()
            disposable = null
        }
    }

}
