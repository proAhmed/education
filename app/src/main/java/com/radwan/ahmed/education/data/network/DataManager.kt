package com.radwan.ahmed.education.data.network

import com.radwan.ahmed.education.data.model.*
import javax.inject.Inject
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Callback


class DataManager @Inject
 constructor(val apiService: ApiInterface) {


    fun getQuestions(country: Int,pageNum: Int): Single<QuestionsResponse> {
        return apiService.getQuestions(country,pageNum)
    }
    fun getMyQuestions(authorId: String,country: Int,pageNum: Int): Single<QuestionsResponse> {
        return apiService.getMyQuestions(authorId,country,pageNum)
    }
    fun getQuestionsDetails(questionId: Int): Single<QuestionDetailsResponse> {
        return apiService.getQuestionDetails(questionId)
    }
    fun getQuestionTest(country: Int,pageNum: Int): Callback<QuestionsResponse> {
        return apiService.getQuestionsTest(country,pageNum)
    }
    fun login(email: String,password: String): Single<UserResponse> {
        return apiService.login(email,password)
    }

    fun register(user: User): Single<UserResponse> {
        return apiService.register(user.userFName,user.userLName,user.userPhone,user.userEmail,user.userPassword,user.userType);
    }
    fun getCategories(): Single<CategoryResponse> {
        return apiService.getCategories()
    }
    fun addQuestions(authorId:RequestBody,categoryId:RequestBody,title: RequestBody, body: RequestBody,file: MultipartBody.Part):Single<AddQuestionResponse> {
        return apiService.addQuestion(authorId,categoryId,title, body,file)
    }
    fun addQuestions(authorId:String,categoryId:Int,title: String, body: String):Single<AddQuestionResponse> {
        return apiService.addQuestion(authorId,categoryId,title, body)
    }
    fun addAnswers(questionId: Int, answer: String,instructorId:String):Single<AddQuestionResponse> {
        return apiService.addAnswer(questionId, answer, instructorId)
    }
    fun addAnswers(questionId: RequestBody, answer: RequestBody,instructorId:RequestBody,file: MultipartBody.Part):Single<AddQuestionResponse> {
        return apiService.addAnswer(questionId, answer, instructorId,file)
    }
    fun getReference(country: Int,pageNum: Int): Single<ReferenceResponse> {
        return apiService.getReference(country,pageNum)
    }
    fun getChapters(referenceId: Int): Single<ChapterResponse> {
        return apiService.getChapters(referenceId)
    }
    fun getRefQuestions(chapterId: Int): Single<RefQuestionsResponse> {
        return apiService.getRefQuestions(chapterId)
    }
    fun getRefQuestion(questionId: Int): Single<RefQuestionResponse> {
        return apiService.getRefQuestion(questionId)
    }
    //    public Observable<NewsModel> getEveryThingNews(String country,String category) {
    //        ApiInterface apiService =
    //                RetrofitClass.getClient().create(ApiInterface.class);
    //        return apiService.getEveryThing(country,category,KeyUtil.API_VALUE);
    //       // return null;
    //    }

    //    public Observable<NewsModel> getEveryThingNews(String country,String category) {
    //        ApiInterface apiService =
    //                RetrofitClass.getClient().create(ApiInterface.class);
    //        return apiService.getEveryThing(country,category,KeyUtil.API_VALUE);
    //        // return null;
    //    }
}
