package com.radwan.ahmed.education.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.radwan.ahmed.education.R
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import butterknife.ButterKnife
import butterknife.Unbinder
import com.radwan.ahmed.education.utils.KeysUtil
import dagger.android.support.DaggerFragment

abstract class BaseFragment : DaggerFragment() {
     var unbinder: Unbinder? = null
    var baseActivity: AppCompatActivity? = null
    @LayoutRes
    protected abstract fun layoutRes(): Int


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(layoutRes(), container, false)
        unbinder = ButterKnife.bind(this, view)
        return view
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        baseActivity = context as AppCompatActivity?
    }

    override fun onDetach() {
        super.onDetach()
        baseActivity = null
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (unbinder != null) {
            unbinder!!.unbind()
            unbinder = null
        }
    }

    fun fragmentMove(fragment: Fragment){
        baseActivity!!.supportFragmentManager.beginTransaction().replace(R.id.container,fragment).commit();
    }
    fun fragmentMove(fragment: Fragment,bundle:Bundle){
        fragment.arguments = bundle
        baseActivity!!.supportFragmentManager.beginTransaction().replace(R.id.container,fragment).commit();
    }
    fun fragmentMove(fragment: Fragment,backStack:String){
        baseActivity!!.supportFragmentManager.beginTransaction().replace(R.id.container,fragment).addToBackStack(backStack).commit();
    }
    fun fragmentMove(fragment: Fragment,bundle:Bundle,backStack:String){
        fragment.arguments = bundle
        baseActivity!!.supportFragmentManager.beginTransaction().replace(R.id.container,fragment).addToBackStack(backStack).commit();
    }


}
