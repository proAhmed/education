package com.radwan.ahmed.education.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.radwan.ahmed.education.data.model.Question
import com.radwan.ahmed.education.data.model.QuestionLocal
import io.reactivex.Observable

@Dao
interface QuestionsDao {

    @Query("SELECT * FROM question")
    fun getQuestions():Observable<List<QuestionLocal>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertQuestion(question: QuestionLocal)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllQuestion(questionList: List<QuestionLocal>)
}