package com.radwan.ahmed.education.di.component


import javax.inject.Scope

@Scope
@kotlin.annotation.Retention
annotation class MyApplicationScope
