package com.radwan.ahmed.education.data.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.radwan.ahmed.education.data.model.Question
import com.radwan.ahmed.education.data.network.DataManager
import io.reactivex.disposables.CompositeDisposable

class NewsDataSourceFactory(private val compositeDisposable: CompositeDisposable,
                            private val networkService: DataManager)
    : DataSource.Factory<Int, Question>() {

    val newsDataSourceLiveData = MutableLiveData<NewsDataSource>()

    override fun create(): DataSource<Int, Question> {
        val newsDataSource = NewsDataSource(networkService, compositeDisposable)
        newsDataSourceLiveData.postValue(newsDataSource)
        return newsDataSource
    }
}
