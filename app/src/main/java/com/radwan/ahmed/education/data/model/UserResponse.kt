package com.radwan.ahmed.education.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class UserResponse(
    val status: String,
    @SerializedName("user")
    val user: User
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readParcelable(Questions::class.java.classLoader)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(status)
        parcel.writeParcelable(user, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<QuestionsResponse> {
        override fun createFromParcel(parcel: Parcel): QuestionsResponse {
            return QuestionsResponse(parcel)
        }

        override fun newArray(size: Int): Array<QuestionsResponse?> {
            return arrayOfNulls(size)
        }
    }
}
