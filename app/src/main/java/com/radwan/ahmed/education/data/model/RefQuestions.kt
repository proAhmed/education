package com.radwan.ahmed.education.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class RefQuestions(
    val id: Int, @SerializedName("content") val content: String,
    @SerializedName("title") val title: String
    , @SerializedName("id_ch") val chapterId: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(content)
        parcel.writeString(title)
        parcel.writeInt(chapterId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RefQuestions> {
        override fun createFromParcel(parcel: Parcel): RefQuestions {
            return RefQuestions(parcel)
        }

        override fun newArray(size: Int): Array<RefQuestions?> {
            return arrayOfNulls(size)
        }
    }
}