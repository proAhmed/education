package com.radwan.ahmed.education.adapter

import android.annotation.SuppressLint
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import java.util.*
import androidx.lifecycle.Observer
import com.radwan.ahmed.education.data.model.Question
import com.radwan.ahmed.education.interfaces.OnClick
import com.radwan.ahmed.education.view_model.QuestionViewModel
import com.radwan.ahmed.education.R
import androidx.annotation.NonNull

class QuestionAdapter(lifecycleOwner: LifecycleOwner, viewModel: QuestionViewModel, val onClickPos: OnClick<Question>)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    private val questionList = ArrayList<Question>()
    private val isLoadingAdded = false
    private val ITEM = 0
    private val LOADING = 1

    class DataObjectHolder(convertView: View) : RecyclerView.ViewHolder(convertView) {
        val tvTitle: TextView = convertView.findViewById(R.id.tvTitle)
        val tvDetails: TextView = convertView.findViewById(R.id.tvDetails)
        val btnAnsweredState:Button = convertView.findViewById(R.id.btnAnsweredState)
    }

    init {
        viewModel.repos.observe(lifecycleOwner, Observer{ questionLists ->
          //  questionList.clear()
            if (questionLists != null) {
                questionList.addAll(questionLists.questions.questions)
                notifyDataSetChanged()
            }
        })
    }

    @NonNull
    private fun getViewHolder(
        parent: ViewGroup,
        inflater: LayoutInflater
    ): RecyclerView.ViewHolder {
        val viewHolder: RecyclerView.ViewHolder
        val v1 = inflater.inflate(R.layout.question_item, parent, false)
        viewHolder = DataObjectHolder(v1)

        return viewHolder
    }


    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null
        val inflater = LayoutInflater.from(parent.context)

            viewHolder = getViewHolder(parent, inflater)

        return viewHolder

    }

    @SuppressLint("NewApi")
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val question = questionList[position]
                val questionHolder = holder as DataObjectHolder
                        holder.tvTitle.text = question.title
                holder.tvDetails.text = question.description
                holder.itemView.setOnClickListener {
                    onClickPos.onClick(question)
                }
                if(question.flagAnswer==0){
                    holder.btnAnsweredState.setCompoundDrawablesWithIntrinsicBounds( R.drawable.close, 0, 0, 0);
                    holder.btnAnsweredState.setBackgroundResource(R.drawable.radius_question)

                }else{
                    holder.btnAnsweredState.setCompoundDrawablesWithIntrinsicBounds( R.drawable.checked, 0, 0, 0);
                }


    }

    override fun getItemCount(): Int {
        return questionList.size
    }
    override fun getItemViewType(position: Int): Int {
        return questionList.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }



}
