package com.radwan.ahmed.education.test

class BaseImp( val x: Int) :Base {
    override val message ="BaseImp x = $x"
    override fun print() {
        println(message)
    }
}