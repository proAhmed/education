package com.radwan.ahmed.education.view

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.radwan.ahmed.education.R
import com.radwan.ahmed.education.adapter.ReferenceQuestionAdapter
import com.radwan.ahmed.education.base.BaseFragment
import com.radwan.ahmed.education.data.model.Chapters
import com.radwan.ahmed.education.data.model.RefQuestions
import com.radwan.ahmed.education.di.helper.ViewModelFactory
import com.radwan.ahmed.education.interfaces.OnClick
import com.radwan.ahmed.education.utils.KeysUtil
import com.radwan.ahmed.education.view_model.RefQuestionViewModel
import com.radwan.ahmed.education.view_model.RefQuestionsViewModel
import kotlinx.android.synthetic.main.question_fragment.*
import javax.inject.Inject


class RefQuestionsScreen : BaseFragment(), OnClick<RefQuestions> {

    @Inject
    lateinit var shared: SharedPreferences
    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    var viewModel: RefQuestionsViewModel? = null
    override fun layoutRes(): Int {
        return R.layout.question_fragment
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val toolbarTitle = baseActivity!!.findViewById<TextView>(R.id.toolbar_title)
       toolbarTitle.text = resources.getString(R.string.reference)
        val bundle = arguments
        if (bundle != null) {
            val category = bundle.getParcelable(KeysUtil.KEY_VALUE) ?: Chapters(0, "",0)

            toolbarTitle.text = category.chapterName
            viewModel =
                ViewModelProviders.of(this, viewModelFactory).get(RefQuestionsViewModel::class.java)
                viewModel!!.getRefQuestions(category.id)

        }
        recyclerView!!.layoutManager = LinearLayoutManager(activity)
        recyclerView!!.adapter = ReferenceQuestionAdapter(this, viewModel!!, this)
        observableViewModel()
    }

      fun observableViewModel() {
        viewModel!!.repos.observe(this, Observer{ newModel ->
            if (newModel != null) {
                recyclerView!!.visibility = View.VISIBLE
            }
        })
        viewModel!!.error.observe(viewLifecycleOwner, Observer{ error ->
            if (error != null) {
                if (error) {
                    tv_error!!.visibility = View.VISIBLE
                    recyclerView!!.visibility = View.GONE
                    tv_error!!.text = "error"
                }
            }
        })

        viewModel!!.getLoading().observe(viewLifecycleOwner, Observer {
            if (it != null) {
                if (it) {
                    loading_view!!.visibility = View.VISIBLE
                    tv_error!!.visibility = View.GONE
                    recyclerView!!.visibility = View.GONE
                } else {
                    loading_view!!.visibility = View.GONE
                }
            }
        })
    }

    override fun onClick(objects: RefQuestions) {
        val bundle = Bundle()
        bundle.putParcelable(KeysUtil.KEY_VALUE, objects)
        val fragment = RefQuestionDetailsScreen()
        fragmentMove(fragment, bundle, "RefQuestion")
    }

}
