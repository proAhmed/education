package com.radwan.ahmed.education.base

import com.radwan.ahmed.education.di.component.ApplicationComponent
import com.radwan.ahmed.education.di.component.DaggerApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class BaseApplication : DaggerApplication(){


      override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val component = DaggerApplicationComponent.builder().application(this).build()
//          val component: ApplicationComponent = DaggerApplicationComponent.builder().build()

          component.inject(this)

        return component
    }
}
