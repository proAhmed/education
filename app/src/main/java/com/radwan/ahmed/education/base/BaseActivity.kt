package com.radwan.ahmed.education.base

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import com.radwan.ahmed.education.R

import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import butterknife.ButterKnife
import com.radwan.ahmed.education.MainActivity
import com.radwan.ahmed.education.utils.KeysUtil
import dagger.android.support.DaggerAppCompatActivity

abstract class BaseActivity<T : Parcelable> : DaggerAppCompatActivity() {
    @LayoutRes
    protected abstract fun layoutRes(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutRes())
        ButterKnife.bind(this)
    }
    fun intentMove(activity: BaseActivity<T>, objectSend:T){
        val intent = Intent(this, activity::class.java)
        intent.putExtra(KeysUtil.KEY_VALUE, objectSend)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }
    fun intentMove(activity: BaseActivity<T>){
        val intent = Intent(this, activity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }
    fun fragmentMove(fragment: Fragment){
        supportFragmentManager.beginTransaction().add(R.id.container,fragment).commit();
    }
    fun fragmentMove(fragment: Fragment,bundle:Bundle){
        fragment.arguments = bundle
        supportFragmentManager.beginTransaction().add(R.id.container,fragment).commit();
    }
    fun fragmentMove(fragment: Fragment,backStack:String){
        supportFragmentManager.beginTransaction().add(R.id.container,fragment).addToBackStack(backStack).commit();
    }
    fun fragmentMove(fragment: Fragment,bundle:Bundle,backStack:String){
        fragment.arguments = bundle
        supportFragmentManager.beginTransaction().add(R.id.container,fragment).addToBackStack(backStack).commit();
    }
}
