package com.radwan.ahmed.education.di.modules

import com.radwan.ahmed.education.view.*
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class MainFragmentBindingModule {

    @ContributesAndroidInjector
    internal abstract fun provideQuestionScreen(): QuestionScreen

    @ContributesAndroidInjector
    internal abstract fun provideCategoryScreen(): CategoryScreen

    @ContributesAndroidInjector
    internal abstract fun provideActionScreen(): ActionScreen

    @ContributesAndroidInjector
    internal abstract fun providAddQuestionScreen(): AddQuestionScreen


    @ContributesAndroidInjector
    internal abstract fun provideAnswerScreen(): AnswerScreen

    @ContributesAndroidInjector
    internal abstract fun provideQuestionDetailsScreen(): QuestionDetailsScreen

    @ContributesAndroidInjector
    internal abstract fun provideReferenceScreen(): ReferenceScreen

    @ContributesAndroidInjector
    internal abstract fun provideChapterScreen(): ChapterScreen

    @ContributesAndroidInjector
    internal abstract fun provideRefQuestionsScreen(): RefQuestionsScreen

    @ContributesAndroidInjector
    internal abstract fun provideRefQuestionDetailsScreen(): RefQuestionDetailsScreen
}
