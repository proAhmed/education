package com.radwan.ahmed.education.view

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import com.radwan.ahmed.education.base.BaseFragment
import com.radwan.ahmed.education.di.helper.ViewModelFactory
import javax.inject.Inject
import com.radwan.ahmed.education.R
import kotlinx.android.synthetic.main.add_question.*
import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Environment
import androidx.lifecycle.Observer
import com.hbisoft.pickit.PickiT
import com.hbisoft.pickit.PickiTCallbacks
import com.radwan.ahmed.education.MainActivity
import com.radwan.ahmed.education.data.model.Category
import com.radwan.ahmed.education.utils.*
import com.radwan.ahmed.education.utils.KeysUtil.Companion.SELECT_FILE
import com.radwan.ahmed.education.view_model.AddQuestionViewModel
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File


class AddQuestionScreen : BaseFragment(), PickiTCallbacks {
    override fun PickiTonStartListener() {

    }

    override fun PickiTonCompleteListener(
        path: String?,
        wasDriveFile: Boolean,
        wasUnknownProvider: Boolean,
        wasSuccessful: Boolean,
        Reason: String?
    ) {
        file = utility.createFileFromPath(path,type)
    }

    override fun PickiTonProgressUpdate(progress: Int) {

    }

    @Inject
lateinit var utility: Utility

    @Inject
    lateinit var validate:Validate
    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var category: Category

    var viewModel: AddQuestionViewModel? = null
    @Inject
    lateinit var sharedPreferences: SharedPreferences
     var file: MultipartBody.Part? = null
    lateinit var pickiT: PickiT
    lateinit var type:String

    override fun layoutRes(): Int {
        return R.layout.add_question
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val toolbarTitle = baseActivity!!.findViewById<TextView>(R.id.toolbar_title)
        toolbarTitle.text = resources.getString(R.string.add_question)
        val bundle = arguments
        pickiT = PickiT(activity, this)

        if (bundle != null) {
             category = arguments!!.getParcelable(KeysUtil.KEY_VALUE) ?: Category(7, "", "")
        }
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(AddQuestionViewModel::class.java)
        btnChooseFile.setOnClickListener{
            chooseFiles()
        }

        btnUpload.setOnClickListener{
            if(validate.validateText(edTitle.text.toString()))

            viewModel!!.addQuestion(sharedPreferences.getString("userId",""), category.categoryId,
                edTitle.text.toString(),edBody.text.toString(),file)
        }
        viewModel!!.resp.observe(this, Observer{ user ->
            if (user.equals("Success", ignoreCase = true)) {

                fragmentMove(CategoryScreen(),user)
            }

        })

    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SELECT_FILE) {
            if (resultCode == Activity.RESULT_OK) {
                type = activity?.contentResolver?.getType(data?.data!!)!!
                pickiT.getPath(data!!.data, Build.VERSION.SDK_INT);


            }
        }
    }

   private fun chooseFiles(){
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*" //allows any image file type. Change * to specific extension to limit it
       intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
       intent.putExtra("return-data", true);
       startActivityForResult(intent, SELECT_FILE);
    }
}
