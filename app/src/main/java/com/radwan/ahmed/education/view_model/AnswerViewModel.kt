package com.radwan.ahmed.education.view_model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.radwan.ahmed.education.data.model.AddQuestionResponse
import com.radwan.ahmed.education.data.model.Question
import com.radwan.ahmed.education.data.model.QuestionDetailsResponse
import com.radwan.ahmed.education.data.network.DataManager
import com.radwan.ahmed.education.utils.Transactions
import com.radwan.ahmed.education.utils.Utility
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MultipartBody
import javax.inject.Inject
import okhttp3.RequestBody




class AnswerViewModel
@Inject
constructor() : ViewModel() {

    @Inject
    lateinit var dataManager: DataManager
    var disposable: CompositeDisposable? = null
    val newsModelMutableLiveData = MutableLiveData<QuestionDetailsResponse>()
    val repoLoadError = MutableLiveData<Boolean>()
    val addQuestionMutableLiveData = MutableLiveData<AddQuestionResponse>()
    val loading = MutableLiveData<Boolean>()
    @Inject
    lateinit var transactions: Transactions<Question>
    @Inject
    lateinit var context: Context
    @Inject
    lateinit var utility: Utility
    val response = MutableLiveData<String>()

    val repos: LiveData<QuestionDetailsResponse>
        get() = newsModelMutableLiveData
    val error: LiveData<Boolean>
        get() = repoLoadError

    init {
        disposable = CompositeDisposable()
        // getData();
    }

    fun getLoading(): LiveData<Boolean> {
        return loading
    }

    fun getData(questionId: Int) {
        loading.value = true
        disposable!!.add(dataManager.getQuestionsDetails(questionId).subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread()).subscribeWith(object :
                DisposableSingleObserver<QuestionDetailsResponse>() {
                override fun onSuccess(value: QuestionDetailsResponse) {
                    repoLoadError.value = false
                    if (!value.status.equals("Failed")) {
                        newsModelMutableLiveData.value = value
                    }
                    loading.value = false
                }

                override fun onError(e: Throwable) {
                    repoLoadError.value = true
                    loading.value = false
                }
            })
        )
    }

    lateinit var datass: Single<AddQuestionResponse>
    fun addAnswer(authorId: String, questionId: Int,  answer: String, file: MultipartBody.Part?) {
        loading.value = true
        //  progressBar.setVisibility(View.VISIBLE);

        if (file != null) {
            datass = dataManager.addAnswers(utility.convertToRequestBody(questionId.toString()),
                utility.convertToRequestBody(answer), utility.convertToRequestBody(authorId),  file)
        } else {
            datass = dataManager.addAnswers(questionId, answer, authorId)

        }
        disposable!!.add(datass.subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread()).subscribeWith(object :
                DisposableSingleObserver<AddQuestionResponse>() {
                override fun onSuccess(value: AddQuestionResponse) {
                    repoLoadError.value = false
                    addQuestionMutableLiveData.value = value
                    loading.value = false
                }

                override fun onError(e: Throwable) {
                    repoLoadError.value = true
                    loading.value = false
                }
            })
        )
    }
    override fun onCleared() {
        super.onCleared()
        if (disposable != null) {
            disposable!!.clear()
            disposable = null
        }
    }

}



