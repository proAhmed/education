package com.radwan.ahmed.education.data.network

import com.radwan.ahmed.education.data.model.*
import com.radwan.ahmed.education.utils.KeysUtil
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*


interface ApiInterface {

    @FormUrlEncoded
    @POST(KeysUtil.KEY_LOGIN)
    fun login(@Field("email")userPhone:String,@Field("password") userPass:String): Single<UserResponse>

    @FormUrlEncoded
    @POST(KeysUtil.KEY_REGISTER)
    fun register(@Field("fname")fName: String,@Field("lname")lName: String,@Field("phone")userPhone:String,
                 @Field("email")userEmail:String,@Field("password")userPass: String
                 ,@Field("type")userType: Int): Single<UserResponse>

    @GET(KeysUtil.KEY_CATEGORIES)
    fun getCategories():Single<CategoryResponse>

    @FormUrlEncoded
    @POST(KeysUtil.KEY_QUESTION)
    fun getQuestions(@Field(KeysUtil.KEY_CATEGORY_ID)categoryId:Int,@Field(KeysUtil.KEY_PAGE_NUM)pageNumber:Int):Single<QuestionsResponse>

    @FormUrlEncoded
    @POST(KeysUtil.KEY_MY_QUESTIONS)
    fun getMyQuestions(@Field(KeysUtil.KEY_AUTHOR_ID)authorId:String,@Field(KeysUtil.KEY_CATEGORY_ID)categoryId:Int,
                       @Field(KeysUtil.KEY_PAGE_NUM)pageNumber:Int):Single<QuestionsResponse>


    @FormUrlEncoded
    @POST(KeysUtil.KEY_QUESTION_DETAILS)
    fun getQuestionDetails(@Field(KeysUtil.KEY_QUESTION_DETAILS_ID)questionId:Int):Single<QuestionDetailsResponse>


    @FormUrlEncoded
    @POST(KeysUtil.KEY_QUESTION)
    fun getQuestionsTest(@Field(KeysUtil.KEY_CATEGORY_ID)categoryId:Int,@Field(KeysUtil.KEY_PAGE_NUM)pageNumber:Int):retrofit2.Callback<QuestionsResponse>

    @Multipart
    @POST(KeysUtil.KEY_ADD_QUESTION)
    fun addQuestion(@Part(KeysUtil.KEY_AUTHOR_ID) authorId:RequestBody,@Part(KeysUtil.KEY_CATEGORY_ID) categoryId:RequestBody,
                    @Part(KeysUtil.KEY_TITLE) questionTitle:RequestBody,@Part(KeysUtil.KEY_DESCRIPTION) questionBody:RequestBody,
                    @Part file: MultipartBody.Part
    )
            :Single<AddQuestionResponse>

    @FormUrlEncoded
    @POST(KeysUtil.KEY_ADD_QUESTION)
    fun addQuestion(@Field(KeysUtil.KEY_AUTHOR_ID) authorId:String,@Field(KeysUtil.KEY_CATEGORY_ID) categoryId:Int,
                    @Field(KeysUtil.KEY_TITLE) questionTitle:String,@Field(KeysUtil.KEY_DESCRIPTION) questionBody:String)
            :Single<AddQuestionResponse>

    @FormUrlEncoded
    @POST(KeysUtil.KEY_ADD_ANSWER)
    fun addAnswer(@Field(KeysUtil.KEY_QUESTION_ID) questionId:Int,@Field(KeysUtil.KEY_ANSWER) answer:String,
                  @Field(KeysUtil.KEY_INSTRUCTOR_ID) instructorId:String):Single<AddQuestionResponse>

    @Multipart
    @POST(KeysUtil.KEY_ADD_ANSWER)
    fun addAnswer(@Part(KeysUtil.KEY_QUESTION_ID) questionId:RequestBody,@Part(KeysUtil.KEY_ANSWER) answer:RequestBody,
                  @Part(KeysUtil.KEY_INSTRUCTOR_ID) instructorId:RequestBody,@Part file: MultipartBody.Part):Single<AddQuestionResponse>

    @FormUrlEncoded
    @POST(KeysUtil.KEY_REFERENCE)
    fun getReference(@Field(KeysUtil.KEY_CATEGORY_ID)categoryId:Int,@Field(KeysUtil.KEY_PAGE_NUM)
    pageNumber:Int):Single<ReferenceResponse>

    @FormUrlEncoded
    @POST(KeysUtil.KEY_REF_CHAPTERS)
    fun getChapters(@Field(KeysUtil.KEY_REFERENCE_ID) referenceId:Int):Single<ChapterResponse>

    @FormUrlEncoded
    @POST(KeysUtil.KEY_REF_QUESTIONS)
    fun getRefQuestions(@Field(KeysUtil.KEY_CHAPTER_ID) chapterId:Int
                        ):Single<RefQuestionsResponse>


    @FormUrlEncoded
    @POST(KeysUtil.KEY_REF_QUESTION)
    fun getRefQuestion(@Field(KeysUtil.KEY_REF_QUESTION_ID) questionId:Int):Single<RefQuestionResponse>
}