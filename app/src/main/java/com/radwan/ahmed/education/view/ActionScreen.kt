package com.radwan.ahmed.education.view

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.radwan.ahmed.education.base.BaseFragment
import com.radwan.ahmed.education.data.model.Category
import kotlinx.android.synthetic.main.action_screen.*
import com.radwan.ahmed.education.R
import com.radwan.ahmed.education.utils.KeysUtil
import javax.inject.Inject

class ActionScreen : BaseFragment() {

    @Inject
    lateinit var shared: SharedPreferences

    override fun layoutRes(): Int {
        return R.layout.action_screen
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val toolbarTitle = baseActivity!!.findViewById<TextView>(R.id.toolbar_title)
        val bundle = arguments

        if (bundle != null) {
            val category = arguments!!.getParcelable(KeysUtil.KEY_VALUE) ?: Category(7, "", "")
            bundle.putParcelable(KeysUtil.KEY_VALUE, category)
            toolbarTitle.text = category.categoryName
            actions(bundle)
        }
    }

    private fun actions(bundle: Bundle) {
        btnReference.setOnClickListener {
            val fragment = ReferenceScreen()
            fragmentMove(fragment, bundle, "Reference")
        }
        btnAddQuestion.setOnClickListener {
            val fragment = AddQuestionScreen()
            fragmentMove(fragment, bundle, "AddQuestion")
        }
        btnListQuestion.setOnClickListener {
            val fragment = QuestionScreen()
            bundle.putString(KeysUtil.KEY_OPEN_SCREEN,"questionsList")
            fragmentMove(fragment, bundle, "Question")
        }
        if(shared.getInt(KeysUtil.KEY_USER_TYPE,0)==3){
            tvMyQuestion.text = getString(R.string.my_answers)
        }
        btnMyQuestion.setOnClickListener {
            val fragment = QuestionScreen()
            bundle.putString(KeysUtil.KEY_OPEN_SCREEN,"MyQuestions")
            fragmentMove(fragment, bundle, "Answers")
        }
    }
}

