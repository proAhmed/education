package com.radwan.ahmed.education.Login

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.radwan.ahmed.education.MainActivity
import com.radwan.ahmed.education.base.BaseActivity
import com.radwan.ahmed.education.di.helper.ViewModelFactory
import com.radwan.ahmed.education.view_model.UserViewModel
import javax.inject.Inject
import com.radwan.ahmed.education.R
import com.radwan.ahmed.education.data.model.User
import com.radwan.ahmed.education.utils.KeysUtil
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_register.edEmail
import kotlinx.android.synthetic.main.activity_register.edPass
import kotlinx.android.synthetic.main.activity_register.loading_view

class RegisterActivity : BaseActivity<User>() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    var viewModel: UserViewModel? = null
    @Inject
    lateinit var shared:SharedPreferences

    override fun layoutRes(): Int {
        return R.layout.activity_register
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel::class.java)
        btnRegister.setOnClickListener{
            val user = userValue()
            viewModel!!.register(user)
            observableViewModel()
        }
        tvLogin.setOnClickListener{
            intentMove(LoginActivity());

        }

    }

    private fun userValue(): User {
        val user = User()
        user.userEmail = edEmail.text.toString()
        user.userFName = edFName.text.toString()
        user.userPassword = edPass.text.toString()
        user.userPhone = edPhone.text.toString()
        user.userLName = edEmail.text.toString()
        if (switchType.isChecked) {
            user.userType = 3
        }
        return user

    }

    private fun observableViewModel() {
        viewModel!!.repos.observe(this, Observer { user ->
            if (!user.userId.isBlank()) {
                shared.edit().putString(KeysUtil.KEY_USER_ID,user.userId).apply()
                shared.edit().putInt(KeysUtil.KEY_USER_TYPE,user.userType).apply()
                finishAffinity()
                intentMove(MainActivity(), user);
            }

        })
        viewModel!!.error.observe(this, Observer { error ->
            if (error != null) {
                if (error) {

                }
            }
        })


        viewModel!!.getLoading().observe(this, Observer {
            if (it != null) {
                if (it) {
                    loading_view!!.visibility = View.VISIBLE
                } else {
                    loading_view!!.visibility = View.GONE

                }
            }
        })
    }

}
