package com.radwan.ahmed.education.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.radwan.ahmed.education.data.model.Category
import io.reactivex.Observable

@Dao
interface CategoriesDao {

    @Query("SELECT * FROM category")
    fun getCategories():Observable<List<Category>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCategory(category: Category)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllCategories(categories: List<Category>)
}