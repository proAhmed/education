package com.radwan.ahmed.education.view

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.radwan.ahmed.education.R
import com.radwan.ahmed.education.adapter.QuestionAdapter
import com.radwan.ahmed.education.base.BaseFragment
import com.radwan.ahmed.education.data.model.Category
import com.radwan.ahmed.education.data.model.Question
import com.radwan.ahmed.education.di.helper.ViewModelFactory
import com.radwan.ahmed.education.interfaces.OnClick
import com.radwan.ahmed.education.utils.KeysUtil
import com.radwan.ahmed.education.utils.PaginationListener
import com.radwan.ahmed.education.view_model.QuestionViewModel
import kotlinx.android.synthetic.main.question_fragment.*
import javax.inject.Inject




class QuestionScreen : BaseFragment(), OnClick<Question> {

    @Inject
    lateinit var shared: SharedPreferences
    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    var pageNum =1
    var viewModel: QuestionViewModel? = null
    override fun layoutRes(): Int {
        return R.layout.question_fragment
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val toolbarTitle = baseActivity!!.findViewById<TextView>(R.id.toolbar_title)
        val bundle = arguments
        if (bundle != null) {
            val category = bundle.getParcelable(KeysUtil.KEY_VALUE) ?: Category(7, "", "")

            toolbarTitle.text = category.categoryName
            viewModel =
                ViewModelProviders.of(this, viewModelFactory).get(QuestionViewModel::class.java)
            if (bundle.getString(KeysUtil.KEY_OPEN_SCREEN, "").equals("questionsList", true)) {
                viewModel!!.getData(category.categoryId,1)
            } else {
                viewModel!!.getMyQuestion(shared.getString(KeysUtil.KEY_USER_ID,"")!!,category.categoryId,1)
            }

            val lin = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            recyclerView!!.layoutManager =  lin
            recyclerView!!.adapter = QuestionAdapter(this, viewModel!!, this)
            recyclerView.addOnScrollListener(object:PaginationListener(lin){

                override fun loadMoreItems() {
                    ++pageNum
                    if (bundle.getString(KeysUtil.KEY_OPEN_SCREEN, "").equals("questionsList", true)) {
                        viewModel!!.getData(category.categoryId,pageNum)
                    } else {
                        viewModel!!.getMyQuestion(shared.getString(KeysUtil.KEY_USER_ID,"")!!,category.categoryId,pageNum)
                    }

                }

                override fun isLastPage(): Boolean {
                    return false
                }

                override fun isLoading(): Boolean {
                    return false
                }

            })

            observableViewModel()
        }
    }

    private fun observableViewModel() {
        viewModel!!.repos.observe(this, Observer { newModel ->
            if (newModel != null) {
                recyclerView!!.visibility = View.VISIBLE
            }
        })
        viewModel!!.error.observe(viewLifecycleOwner, Observer { error ->
            if (error != null) {
                if (error) {
                    tv_error!!.visibility = View.VISIBLE
                    recyclerView!!.visibility = View.GONE
                    tv_error!!.text = "error"
                }
            }
        })

        viewModel!!.getLoading().observe(viewLifecycleOwner, Observer {
            if (it != null) {
                if (it) {
                    loading_view!!.visibility = View.VISIBLE
                    tv_error!!.visibility = View.GONE
                    recyclerView!!.visibility = View.GONE
                } else {
                    loading_view!!.visibility = View.GONE
                }
            }
        })
    }

    override fun onClick(objects: Question) {
        val bundle = Bundle()
        bundle.putParcelable(KeysUtil.KEY_VALUE, objects)
        val fragment = AnswerScreen()
        fragmentMove(fragment, bundle, "Question")
    }

}
