package com.radwan.ahmed.education.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.squareup.moshi.Json

data class CategoryResponse(val status:String,
                            @SerializedName("categories")
                            val list:List<Category>):Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.createTypedArrayList(Category)
    ) {
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<CategoryResponse> {
        override fun createFromParcel(parcel: Parcel): CategoryResponse {
            return CategoryResponse(parcel)
        }

        override fun newArray(size: Int): Array<CategoryResponse?> {
            return arrayOfNulls(size)
        }
    }
}