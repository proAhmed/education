package com.radwan.ahmed.education.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Reference(
      @SerializedName("id_ref") val refId: Int
    , @SerializedName("name_ref") val refName: String
    , @SerializedName("image") val image: String
    , @SerializedName("content_en") val contentEn: String
    , @SerializedName("content_ar") val contentAr: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(refId)
        parcel.writeString(refName)
        parcel.writeString(image)
        parcel.writeString(contentEn)
        parcel.writeString(contentAr)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Reference> {
        override fun createFromParcel(parcel: Parcel): Reference {
            return Reference(parcel)
        }

        override fun newArray(size: Int): Array<Reference?> {
            return arrayOfNulls(size)
        }
    }
}