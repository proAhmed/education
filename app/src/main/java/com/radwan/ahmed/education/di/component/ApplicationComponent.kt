package com.radwan.ahmed.education.di.component

import android.app.Application
import androidx.appcompat.app.AppCompatActivity
import com.radwan.ahmed.education.MainActivity
import com.radwan.ahmed.education.base.BaseActivity
import com.radwan.ahmed.education.base.BaseApplication
import com.radwan.ahmed.education.di.modules.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dagger.android.support.DaggerApplication

//,BaseActivityBindingModule::class
//,RoomModule.class
@Component(modules = arrayOf(ContextModule::class, NetworkModule::class, AndroidSupportInjectionModule::class,
     ActivityBindingModule::class,ViewModelFactoryModule::class, ViewModelModule::class))
@MyApplicationScope
interface ApplicationComponent : AndroidInjector<DaggerApplication> {
    fun inject(baseApplication: BaseApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }


}
