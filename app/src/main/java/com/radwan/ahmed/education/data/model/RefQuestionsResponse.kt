package com.radwan.ahmed.education.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class RefQuestionsResponse(val status:String,
                                @SerializedName("questions")
                            val refQuestions: List<RefQuestions>):Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.createTypedArrayList(RefQuestions)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(status)
        parcel.writeTypedList(refQuestions)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RefQuestionsResponse> {
        override fun createFromParcel(parcel: Parcel): RefQuestionsResponse {
            return RefQuestionsResponse(parcel)
        }

        override fun newArray(size: Int): Array<RefQuestionsResponse?> {
            return arrayOfNulls(size)
        }
    }
}

