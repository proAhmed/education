package com.radwan.ahmed.education.data.model

enum class State {
    DONE, LOADING, ERROR
}